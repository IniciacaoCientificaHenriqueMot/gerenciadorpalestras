import React, { useState, useEffect } from "react";
import { Table } from "semantic-ui-react";
import firebase from "../../service/api";
import Skeleton from "react-loading-skeleton";
import { FaRegFileExcel, FaEye, FaQrcode } from "react-icons/fa";
import { Modal, Button } from "../../../node_modules/react-bootstrap";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";

const SkeletonLine = () => <Skeleton />;

const PalestrasList = () => {
  const [lista, setLIsta] = useState([]);
  const [listaInscritos, setListaInscritos] = useState([]);
  const [loading, setLoading] = useState(false);

  const [showLista, setShowLista] = useState(false);
  const [listaImpressao, setListaImpressao] = useState([]);

  const dispatch = useDispatch();

  const handleCloseLista = () => setShowLista(false);
  const handleShowLista = () => setShowLista(true);

  useEffect(() => {
    const db = firebase.firestore();
    db.collection("palestras").onSnapshot(spnapshot => {
      let lista = [];
      spnapshot.forEach(doc => {
        lista.push({
          key: doc.id,
          palestrante: doc.data().palestrante,
          titulo: doc.data().titulo,
          dataInicio: doc
            .data()
            .dataInicio.toDate()
            .toLocaleString()
        });
      });
      setLoading(true);
      setLIsta(lista);
    });
  }, []);

  useEffect(() => {
    const db = firebase.firestore();
    db.collection("inscritos").onSnapshot(spnapshot => {
      let inscritos = [];
      spnapshot.forEach(doc => {
        inscritos.push({
          key: doc.id,
          nome: doc.data().nome,
          telefone: doc.data().telefone,
          titulo: doc.data().titulo
        });
      });
      setLoading(true);
      setListaInscritos(inscritos);
    });
  }, []);

  function handleDispatch(item) {
    let lista = item;
    dispatch({ type: "ADD_PALESTRAS", lista });
  }

  const preview = (item, e) => {
    let previewValue = listaInscritos.filter(data => data.titulo === item);
    setListaImpressao(previewValue);
    setShowLista(true);
  };

  const convertToCSV = (objArray, e) => {
    var array = typeof objArray != "object" ? JSON.parse(objArray) : objArray;
    var str = "";
    var row = "";
    var filename = "listaInscritos";

    //separado por vírgulas
    for (var index in objArray[0]) {
      row += index + ";";
    }
    row = row.slice(0, -1);
    str += row + "\r\n";

    for (var i = 0; i < array.length; i++) {
      var line = "";
      for (var index in array[i]) {
        if (line != "") {
          line += ";";
        }
        line += array[i][index];
      }
      str += line + "\r\n";
    }

    var a = document.createElement("a");
    a.setAttribute("style", "display:none;");
    document.body.appendChild(a);
    var blob = new Blob([str], { type: "text/csv" });
    var url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = filename.length > 0 ? filename + ".csv" : "export.csv";
    a.click();
  };

  const createList = () => {
    let profileContent = null;

    if (loading === false) {
      profileContent = (
        <Table.Row>
          <Table.Cell>
            <SkeletonLine duration={50} />
          </Table.Cell>
          <Table.Cell>
            <SkeletonLine duration={50} />
          </Table.Cell>
          <Table.Cell>
            <SkeletonLine duration={50} />
          </Table.Cell>
          <Table.Cell>
            <SkeletonLine duration={50} />
          </Table.Cell>
          <Table.Cell>
            <SkeletonLine duration={50} />
          </Table.Cell>
        </Table.Row>
      );
    } else {
      profileContent = lista.map(item => (
        <Table.Row key={item.key}>
          <Table.Cell>{item.titulo}</Table.Cell>
          <Table.Cell>
            {listaInscritos.filter(data => data.titulo === item.titulo).length}
          </Table.Cell>
          <Table.Cell>{item.dataInicio}</Table.Cell>
          <Table.Cell>
            <Button
              variant="primary"
              onClick={e => preview(item.titulo, e)}
              className="mr-2"
            >
              <FaEye />
            </Button>
            <Button
              variant="success"
              onClick={e =>
                convertToCSV(
                  listaInscritos.filter(data => data.titulo === item.titulo),
                  e
                )
              }
              className="mr-2"
            >
              <FaRegFileExcel />
            </Button>
            <Link to={{ pathname: `/impressao` }}>
              <Button
                variant="secondary"
                className="mr-2"
                onClick={e => handleDispatch(JSON.stringify(item), e)}
              >
                <FaQrcode />
              </Button>
            </Link>
          </Table.Cell>
        </Table.Row>
      ));
    }
    return profileContent;
  };

  return (
    <div className="container mt-3">
      <Table columns={6}>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Palestra</Table.HeaderCell>
            <Table.HeaderCell>Qtd inscritos</Table.HeaderCell>
            <Table.HeaderCell>Data limite</Table.HeaderCell>
            <Table.HeaderCell>Ações</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>{createList()}</Table.Body>
        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell>Quantidade: {lista.length}</Table.HeaderCell>
            <Table.HeaderCell />
            <Table.HeaderCell colSpan="3"></Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>

      <Modal show={showLista} onHide={handleCloseLista}>
        <Modal.Header closeButton>
          <Modal.Title>lista de inscritos</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Table columns={5}>
            <Table.Header>
              <Table.Row>
                <Table.HeaderCell>Nome</Table.HeaderCell>
                <Table.HeaderCell>Telefone</Table.HeaderCell>
              </Table.Row>
            </Table.Header>
            <Table.Body>
              {listaImpressao.map(x => (
                <Table.Row key={x.key}>
                  <Table.Cell>{x.nome}</Table.Cell>
                  <Table.Cell>{x.telefone}</Table.Cell>
                </Table.Row>
              ))}
            </Table.Body>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseLista}>
            Fechar
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default PalestrasList;
