import React, { useState, useEffect } from "react";
import { Link, Redirect } from "react-router-dom";
import { FaSignOutAlt, FaBars } from "react-icons/fa";
import firebase from "../../service/api";
import { Modal, Button } from "../../../node_modules/react-bootstrap";

import "./styles.css";
function Header(props) {
  const [redirect, setRedirect] = useState(false);
  const [usuario, setUsuario] = useState("");
  useEffect(() => {
    firebase.auth().onAuthStateChanged(user => {
      setUsuario(user.email);
    });
  }, []);

  function logout() {
    firebase.auth().signOut();

    setRedirect(true);
  }

  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <nav
      id="navHeader"
      className=" d-flex p-2 "
      style={{ backgroundColor: "#1E90FF" }}
    >
      <Button
        variant="outline-primary mr-3"
        onClick={handleShow}
        style={{ border: "none" }}
      >
        <FaBars className="text-white" />
      </Button>
      <Modal show={show} onHide={handleClose} className="left">
        <Modal.Header closeButton>
          <Modal.Title>
            <h3>Usuário:</h3>
            <h6>{usuario}</h6>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ul className="list-group list-group-flush">
            <li className="list-group-item">
              <Link to="/cadastro-palestra">Cadastro de palestras</Link>
            </li>
            <li className="list-group-item">
              <Link to="/lista-palestra">Lista de palestras</Link>
            </li>
            <li className="list-group-item"></li>
          </ul>
        </Modal.Body>
      </Modal>
      <div className="navbar-text main-text navName">{props.name}</div>
      <div style={{ marginLeft: "auto" }}>
        <Button
          variant="outline-primary"
          onClick={logout}
          style={{ border: "none" }}
        >
          <FaSignOutAlt className="text-white" />
        </Button>
        {redirect && <Redirect to="/" />}
      </div>
    </nav>
  );
}
export default Header;
