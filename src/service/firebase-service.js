import { firebaseFirestore, firebaseAuth } from "./api";

export default class FirebaseService {
  static getData(path, callback, order = null, size = 0) {
    let query = firebaseFirestore.collection(path);
    if (order) query = query.orderBy(order);
    if (size > 0) query = query.limit(size);

    query.onSnapshot(dataSnapshot => {
      let items = [];
      dataSnapshot.forEach(childSnapshot => {
        let item = childSnapshot.data();
        item["id"] = childSnapshot.id;
        items.push(item);
      });
      callback(items);
    });

    return query;
  }

  static addData(node, obj, callback) {
    firebaseFirestore
      .collection(node)
      .add(obj)
      .then(e => callback(e))
      .catch(error => callback(error));
  }

  static login(user, callback) {
    firebaseAuth
      .signInWithEmailAndPassword(user.email, user.password)
      .then(e => callback(e))
      .catch(error => callback(error));
  }

  static register(user, callback) {
    firebaseAuth
      .createUserWithEmailAndPassword(user.email, user.password)
      .then(e => callback(e))
      .catch(error => callback(error));
  }

  static singOut() {
    firebaseAuth.singOut();
  }
}
