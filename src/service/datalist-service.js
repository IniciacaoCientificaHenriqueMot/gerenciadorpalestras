export default class DatalistService {
  static getOptionsFromArray(data, field = "titulo") {
    return data.map((e, i) => ({ value: e, label: e[field] }));
  }

  static getOptionsFromArrayUsuarioCadastrado(data, field = "nome") {
    return data.map((e, i) => ({ value: e, label: e[field] }));
  }
}
