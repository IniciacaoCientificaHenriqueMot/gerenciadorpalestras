import firebase from 'firebase'
import 'firebase/firestore'
import 'firebase/auth'

var firebaseConfig = {
	apiKey: 'AIzaSyC_iadbd5IkHkrUiECDGClcpqxfiSfvuWY',
	authDomain: 'palestras-56ee5.firebaseapp.com',
	databaseURL: 'https://palestras-56ee5.firebaseio.com',
	projectId: 'palestras-56ee5',
	storageBucket: 'palestras-56ee5.appspot.com',
	messagingSenderId: '534700370502',
	appId: '1:534700370502:web:34197d0b7a89e9ddc37847',
}

export const firebaseImpl = firebase.initializeApp(firebaseConfig)
export const firebaseDatabase = firebase.database()
export const firebaseFirestore = firebase.firestore()
export const firebaseAuth = firebase.auth()

// firebase.initializeApp(firebaseConfig)
export default firebase
