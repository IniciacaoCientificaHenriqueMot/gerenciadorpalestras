const intialState = {
  data: "portal.estacio.br"
};
export default function Palestras(state = intialState, action) {
  switch (action.type) {
    case "ADD_PALESTRAS":
      return {
        ...state,
        data: action.lista
      };
    default:
      return state;
  }
}
