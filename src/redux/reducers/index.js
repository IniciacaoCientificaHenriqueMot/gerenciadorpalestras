import { combineReducers } from "redux";

import Palestras from "./palestra-reducer";

export default combineReducers({
  palestras: Palestras
});
