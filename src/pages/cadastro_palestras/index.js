import React, { useState, useEffect } from "react";
import "./style.css";
import firebase from "../../service/api";
import { FirebaseService, DatalistService } from "../../service";
import { Link } from "react-router-dom";
import { FaUser } from "react-icons/fa";
import { toast, ToastContainer } from "react-toastify";
import Select from "react-select";

export default function Login() {
  const [lista, setLista] = useState([]);
  const [itemLista, setItemLista] = useState(null);
  const [inscritos, setInscritos] = useState({});
  const [nome, setNome] = useState("");
  const [telefone, setTelefone] = useState("");
  const [palestra, setPalestra] = useState("");

  useEffect(() => {
    FirebaseService.getData(
      "palestras",
      data => setLista(DatalistService.getOptionsFromArray(data)),
      "titulo"
    );
  }, []);

  useEffect(() => {
    const db = firebase.firestore();
    db.collection("inscritos").onSnapshot(spnapshot => {
      let inscritos = [];
      spnapshot.forEach(doc => {
        inscritos.push({
          key: doc.id,
          nome: doc.data().nome,
          telefone: doc.data().telefone,
          titulo: doc.data().titulo
        });
      });
      setInscritos(inscritos);
    });
  }, []);

  function handleSubmit(event) {
    event.preventDefault();
    inscritos.map(x => {
      if (
        x.telefone === telefone &&
        x.titulo === palestra.titulo &&
        x.nome === nome
      ) {
        toast.error("Você ja está telefonedo nessa palestra!", {
          position: toast.POSITION.BOTTOM_CENTER
        });
      } else {
        const db = firebase.firestore();
        db.collection("inscritos").add({
          nome: nome,
          telefone: telefone,
          titulo: palestra.titulo
        });
        toast.success("inscrição realizada com sucesso!", {
          position: toast.POSITION.BOTTOM_CENTER
        });
      }
    });
    handleReset();
  }

  function handleReset() {
    setNome("");
    setTelefone("");
    setPalestra(itemLista);
  }

  return (
    <div>
      <div className="main-icon mt-3 ml-4">
        <Link to="/">
          <FaUser />
        </Link>
      </div>
      <div className="fundo-inner pt-5">
        <div
          style={{
            textAlign: "center",
            justifyContent: "center"
          }}
        >
          <h1
            style={{
              color: "#fff",
              fontWeight: "bold",
              fontSize: "25px"
            }}
          >
            Faça aqui sua inscrição e aproveite o melhor do evento
          </h1>
        </div>
        <form className=" text-center pt-5" onSubmit={handleSubmit.bind(this)}>
          <input
            type="text"
            id="nome"
            placeholder="Nome Completo"
            className="form-control mb-4"
            onChange={e => setNome(e.target.value)}
            value={nome}
            required
          />
          <input
            type="text"
            id="telefone"
            placeholder="Telefone"
            className="form-control mb-4"
            onChange={e => setTelefone(e.target.value)}
            value={telefone}
            required
          />

          <Select
            value={itemLista}
            options={lista}
            onChange={option => {
              setItemLista(option);
              setPalestra(option.value);
            }}
            placeholder="Palestra desejada"
          />

          <div className="mt-5">
            <input
              type="submit"
              value="Cadastrar"
              className="btn btn-success"
            />
            <ToastContainer autoClose={3000} />
          </div>
        </form>
      </div>
    </div>
  );
}
