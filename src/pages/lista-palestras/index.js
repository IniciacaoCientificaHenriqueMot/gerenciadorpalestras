import React, { Component } from 'react'
import Header from '../../components/header'

import PalestraList from '../../components/palestra-list'
export default class ListaPalestras extends Component {
	render() {
		return (
			<div>
				<Header name='Palestras' bg='#279566' />
				<PalestraList />
			</div>
		)
	}
}
