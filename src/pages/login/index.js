import React, { useState, useEffect } from "react";
import "./style.css";
import firebase from "../../service/api";
import { Link, Redirect } from "react-router-dom";
import { FaAddressCard } from "react-icons/fa";
import { toast, ToastContainer } from "react-toastify";

export default function Login() {
  const [email, setEmail] = useState("");
  const [senha, setSenha] = useState("");
  const [redirect, setRedirect] = useState(false);

  useEffect(() => {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        toast.success("Seja bem vindo(a) :)", {
          position: toast.POSITION.BOTTOM_CENTER
        });
        setRedirect(true);
      }
    });
  }, []);

  function logar(e) {
    e.preventDefault();
    firebase
      .auth()
      .signInWithEmailAndPassword(email, senha)
      .catch(error => {
        if (error.code === "auth/wrong-password") {
          toast.error("Senha ou login incorretos  :(", {
            position: toast.POSITION.BOTTOM_CENTER
          });
          setEmail("");
          setSenha("");
        }
        if (error.code === "auth/user-not-found") {
          toast.error("Usuário não encontrado  :(", {
            position: toast.POSITION.BOTTOM_CENTER
          });
          setEmail("");
          setSenha("");
        }
        if (error.code === "auth/invalid-email") {
          toast.error("Email inválido  :(", {
            position: toast.POSITION.BOTTOM_CENTER
          });
          setEmail("");
          setSenha("");
        }
      });
    setEmail("");
    setSenha("");
    setRedirect(true);
  }

  return (
    <div>
      <div className="main-icon mt-3 ml-4">
        <Link to="/cadastro-aluno">
          <FaAddressCard />
        </Link>
      </div>
      <div className="fundo-inner  justify-content-center p-3">
        <div className="justify-content-center align-items-center d-flex mt-1">
          <img alt="logo" src={require("../../assets/discourse.png")} />
        </div>
        <form className=" text-center pt-5" onSubmit={logar}>
          <input
            type="text"
            id="email"
            value={email}
            placeholder="Email"
            className="form-control mb-4"
            onChange={e => setEmail(e.target.value)}
            required
          />
          <input
            type="password"
            id="senha"
            placeholder="Senha"
            value={senha}
            className="form-control mb-4"
            onChange={e => setSenha(e.target.value)}
            required
          />
          <div className="row justify-content-center align-items-center">
            <div className="mt-3 mr-3">
              <input type="submit" value="Entrar" className="btn btn-success" />
            </div>
            <div className="mt-3">
              <Link to="/cadastro">
                <input
                  type="submit"
                  value="Cadastrar"
                  className="btn btn-info"
                />
              </Link>
            </div>
            {redirect === true && <Redirect to="/cadastro-palestra" />}
          </div>
        </form>
      </div>
      {redirect && <ToastContainer autoClose={3000} />}
    </div>
  );
}
