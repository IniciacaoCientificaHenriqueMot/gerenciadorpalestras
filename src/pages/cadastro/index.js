import React, { Component } from "react";
import "./style.css";
import firebase from "../../service/api";
import { Redirect } from "react-router-dom";
import { toast, ToastContainer } from "react-toastify";

export default class Cadastro extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emailInput: "",
      senhaInput: "",
      redirect: false
    };
    firebase.auth().signOut();
  }

  handleChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  cadastrar = e => {
    e.preventDefault();
    firebase
      .auth()
      .createUserWithEmailAndPassword(
        this.state.emailInput,
        this.state.senhaInput
      )
      .then(data => {
        console.log(data);
        this.setState({ emailInput: "", senhaInput: "", redirect: true });
      })
      .catch(error => {
        if (error.code === "auth/weak-password") {
          toast.error("Sua senha deve ter no mínimo 6 caracteres", {
            position: toast.POSITION.BOTTOM_CENTER
          });
          this.setState({ redirect: false });
        } else if (error.code === "auth/email-already-in-use") {
          toast.error("Email já em uso!", {
            position: toast.POSITION.BOTTOM_CENTER
          });
          this.setState({ redirect: false });
        }
      });
  };

  render() {
    return (
      <div>
        <div className="d-flex justify-content-center align-items-center">
          <div className="card mt-5 w-75 fundo-card bg-teal">
            <div className="card-header text-center">
              <h3>Cadastre-se!</h3>
            </div>
            <div className="card-body">
              <div className="fundo-inner  justify-content-center p-3">
                <div className="justify-content-center align-items-center d-flex mt-1"></div>
                <form className=" text-center pt-1" onSubmit={this.cadastrar}>
                  <input
                    type="text"
                    id="emailInput"
                    value={this.state.emailInput}
                    placeholder="Email para cadastro"
                    className="form-control mb-4"
                    onChange={this.handleChange}
                    required
                  />
                  <input
                    type="password"
                    id="senhaInput"
                    placeholder="Senha para cadastro"
                    value={this.state.senhaInput}
                    className="form-control mb-4"
                    onChange={this.handleChange}
                    required
                  />
                  <div className="row justify-content-center align-items-center">
                    <div className="mt-3 mr-3 ">
                      <input
                        type="submit"
                        value="Salvar"
                        className="btn btn-success"
                      />
                    </div>
                    <div className="mt-3 mr-3 ">
                      <a
                        href="/"
                        className="btn btn-warning"
                        style={{ color: "#FFF" }}
                      >
                        Voltar
                      </a>
                    </div>
                    {this.state.redirect && <Redirect to="/" />}
                    <ToastContainer autoClose={3000} />
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
