import React, { useState } from "react";
import { useSelector } from "react-redux";
import Header from "../../components/header";
import QRCode from "../../components/qrcode";
import { FaPrint } from "react-icons/fa";

const Impressao = () => {
  const [qrcode, setQrCode] = useState([]);
  const arrayPalestras = useSelector(state => state.palestras.data);

  return (
    <div>
      <Header bg="#279566" name="Impressão do QR_Code" />
      <div>
        <a
          onClick={window.print}
          style={{
            color: "white",
            display: "flex",
            float: "right",
            marginRight: "20px",
            marginTop: "20px"
          }}
        >
          <FaPrint />
        </a>
      </div>
      <h1 style={{ color: "#FFF" }}>{}</h1>
      <div className="container p-3">
        <div className=" p-3 form-row d-flex justify-content-center">
          <div className="card w-50 p-5">
            <QRCode style={{ width: "100%" }} array={arrayPalestras} />
          </div>
        </div>
      </div>
    </div>
  );
};

export default Impressao;
