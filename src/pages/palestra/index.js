import React, { useState } from "react";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import firebase from "../../service/api";

import QRCode from "../../components/qrcode";
import Header from "../../components/header";
import DatePicker from "../../../node_modules/react-datepicker";

import { toast, ToastContainer } from "react-toastify";

function Palestra() {
  const [key] = useState("60f0f53479393fbd9d3af6e16c1929aa");
  const [titulo, setTitulo] = useState("");
  const [palestrante, setPalestrante] = useState("");
  const [local, setLocal] = useState("");
  const [dataInicio, setDataInicio] = useState(new Date());
  const [validade, setValidade] = useState(new Date());
  const [qrCode, setQrCode] = useState("portal.estacio.br");

  const dispatch = useDispatch();

  const handleSubmit = e => {
    e.preventDefault();
    const db = firebase.firestore();
    db.collection("palestras").add({
      key,
      palestrante,
      titulo,
      local,
      dataInicio,
      validade
    });
    let lista = [];
    lista.push({
      palestrante,
      titulo,
      local,
      dataInicio,
      validade,
      key
    });
    dispatch(handleDispatch(lista));
    setQrCode(lista);
  };

  function handleDispatch(lista) {
    return { type: "ADD_PALESTRAS", lista };
  }

  function handleNew() {
    return (
      setPalestrante(""),
      setTitulo(""),
      setLocal(""),
      setDataInicio(new Date()),
      setValidade(new Date())
    );
  }

  return (
    <div>
      <Header name="Palestras" bg="#279566" />
      <div className="container p-3">
        <div className="card mb-3 p-4 form-row" style={{ minHeight: 400 }}>
          <div className="form-row">
            <div className="col-md-8">
              <form onSubmit={handleSubmit}>
                <div className="title mb-1">
                  <label>Palestrantes:</label>
                </div>
                <div>
                  <input
                    type="text"
                    className="form-control"
                    id="palestrante"
                    onChange={e => setPalestrante(e.target.value)}
                    value={palestrante}
                    required
                  />
                </div>
                <div className="title mb-1 mt-2">
                  <label>Título:</label>
                </div>
                <div>
                  <input
                    type="text"
                    className="form-control"
                    id="titulo"
                    onChange={e => setTitulo(e.target.value)}
                    value={titulo}
                    required
                  />
                </div>
                <div className="title  mb-1 mt-2">
                  <label>Local:</label>
                </div>
                <div>
                  <input
                    type="text"
                    className="form-control"
                    id="local"
                    onChange={e => setLocal(e.target.value)}
                    value={local}
                    required
                  />
                </div>
                <div className="row mt-2">
                  <div className="col-6 mb-3">
                    <div className="title  mb-1 ">
                      <label>Início das Inscrições</label>
                    </div>
                    <div>
                      <DatePicker
                        className="form-control"
                        selected={dataInicio}
                        showTimeSelect
                        onChange={dataInicio => {
                          if (dataInicio < new Date() - 1) {
                            toast.error(
                              "A data ou hora da palestra não podem ser menor do que a data atual",
                              {
                                position: toast.POSITION.BOTTOM_CENTER
                              }
                            );
                          } else {
                            setDataInicio(dataInicio);
                          }
                        }}
                        dateFormat="dd/MM/yyyy"
                      />
                    </div>
                  </div>
                  <div className="col-6 mb-3">
                    <div className="title  mb-1 ">
                      <label>Validade das Inscrições</label>
                    </div>
                    <div>
                      <DatePicker
                        className="form-control"
                        selected={validade}
                        showTimeSelect
                        onChange={validade => {
                          if (validade < new Date() - 1) {
                            toast.error(
                              "A data ou hora do QR_Code não pode ser menor do que a data atual",
                              {
                                position: toast.POSITION.BOTTOM_CENTER
                              }
                            );
                          } else {
                            setValidade(validade);
                          }
                        }}
                        dateFormat="dd/MM/yyyy"
                      />
                    </div>
                  </div>
                </div>
                <div className="mt-3 ml-1">
                  <input
                    type="submit"
                    className="btn btn-success"
                    value="Gerar"
                    onClick={handleSubmit}
                  />
                  <input
                    type="submit"
                    className="btn btn-primary ml-2"
                    value="Novo"
                    onClick={handleNew}
                  />
                </div>
              </form>
            </div>

            <div className="col-md-4 mb-2">
              <h3 className="text-center mt-3"> QRCode</h3>
              <div className="p-1 text-center">
                <Link to={{ pathname: `/impressao` }}>
                  <QRCode
                    style={{ width: "77%", alignSelf: "center" }}
                    array={JSON.stringify(qrCode)}
                  />
                </Link>
              </div>
              <div className="p-1 d-flex justify-content-center">
                <label className="text-center  w-50">
                  <h4>Clique no Qr_Code para visualizar ou imprimir!</h4>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
      {<ToastContainer autoClose={3000} />}
    </div>
  );
}

export default Palestra;
