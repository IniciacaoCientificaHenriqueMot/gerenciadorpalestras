import "../node_modules/popper.js/dist/popper.js";
import "../node_modules/react-toastify/dist/ReactToastify.css";
import "jquery/dist/jquery.min.js";
import "bootstrap/dist/css/bootstrap.min.css";
import "semantic-ui-css/semantic.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import "./styles.css";
import "../node_modules/react-datepicker/dist/react-datepicker.css";

import React, { useEffect, useState } from "react";
import firebase from "./service/api";

import { Provider } from "react-redux";

import { BrowserRouter, Route, Switch } from "react-router-dom";
import Palestra from "./pages/palestra";

import store from "./redux";
import Login from "./pages/login";
import Cadastro from "./pages/cadastro";
import Impressao from "./pages/impressao";
import ListaPalestras from "./pages/lista-palestras";
import cadastroPalestra from "./pages/cadastro_palestras";

export default function App() {
  const [logado, setLogado] = useState(false);

  useEffect(() => {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        setLogado(true);
      } else {
        setLogado(false);
      }
    });
  }, []);

  return <Logged logado={logado} />;

  function Logged(props) {
    const { logado } = props;
    if (logado) {
      return (
        <>
          <Provider store={store}>
            <BrowserRouter>
              <Switch>
                <Route exact path="/cadastro-palestra" component={Palestra} />
                <Route
                  exact
                  path="/lista-palestra"
                  component={ListaPalestras}
                />
                <Route exact path="/impressao" component={Impressao} />
              </Switch>
            </BrowserRouter>
          </Provider>
        </>
      );
    } else {
      return (
        <Provider store={store}>
          <BrowserRouter>
            <Switch>
              <Route exact path="/" component={Login} />
              <Route exact path="/cadastro" component={Cadastro} />
              <Route
                exact
                path="/cadastro-aluno"
                component={cadastroPalestra}
              />
              <Route path="*" component={Login} />
            </Switch>
          </BrowserRouter>
        </Provider>
      );
    }
  }
}
